/*
Structure of a Live Market::

-------------------------------------Market------------------------------
Contains trading rates - (Traditionally these are time dependent, but fixed for now) 
double spot rate, LIBOR rate; 
Conatins live stocks - (data structure like Linked List) [Companies - their stock value]


------------------------------------Broker-------------------------------
He has options contracts to sell and buy. Call/put/swap options
He also decides how to price them, so he controls the pricing functions and does not 
show them to the user.
Hence he needs the info in the market and then asks for K, T (Strike price and Time) 
from the user.
He is also a good salesman, so he shows a catalogue of (call/put/swap) prices 
available for various possible values of K and T.

------------------------------------User---------------------------------
Looks at the catalogue presented by the, then tells his choice of Strike and Time 
to the broker. Broker computes the price of the contract with these desired parameters 
and shows it to him.

---------------------------------Conclusion------------------------------------
This is a static, stuck in time, baby model.
To make it come alive, spot, libor rates and the stock value of the companies 
(in the linked list) must be updated daily and automatically from a webpage.
Admit the excel/csv file of various parameters, compute prices for all the rows of data.
Write the prices for all the rows of the input data into an output csv file.
Use some mathplot library to display it graphically as a sales catalogue. 

*/


#pragma once
#include<iostream>
#include<vector>

struct Futures
{
	int year = 0;
	double yield = 0;
	double forward = 0;
	double bond_price = 0;
	double swap_rate = 0;
	double coupon_value = 0;
	//int current_year = 2020;

};

class Market
{
public:
	/*double yield_quotes[12], forward_quotes[12], swap_quotes[12] = { 0 };//ideally these have to be lists with node being a date stamp and value being the rate on that day
	double time_intervals[12], coupon_rates[12] = { 0 };
	double zero_coupon_bond_price[12] = { 0 };*/

	std::vector<Futures> rates;

	Market(int year_count); //constructor;
	
	// void yield_to_others();
	void display();


};
