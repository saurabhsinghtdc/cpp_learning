#include<math.h>
#include<iostream>
#include<iomanip>
#include "Market.h"

Market::Market(int year_count)
{
    double current_yield = 0.03;
    for (int i = 0; i < year_count; i++)
    {
        Futures entry = Futures();
        entry.year = i + 1;
        entry.yield = current_yield;
        entry.bond_price = exp(-1 * entry.year * entry.yield);
        entry.coupon_value = rand() % 5;
        rates.push_back(entry);

        current_yield += 0.0054;
    }

    for (int i = 0; i < rates.size(); i++)
    {
        Futures& future = rates[i];

        int time_diff = future.year;
        double bond_price = future.bond_price / exp(-1 * (future.year + 1) * current_yield);
        if (i != rates.size() - 1)
        {
            time_diff = rates[i + 1].year - future.year;
            bond_price = future.bond_price / rates[i + 1].bond_price;
        }

        future.forward = (1.0 / time_diff) * (bond_price - 1);

        double sum = 0.0;
        for (int j = 0; j < i; j++)
        {
            sum += rates[j].bond_price;
        }
        future.swap_rate = (future.bond_price * future.forward) / (sum == 0 ? 1.0 : sum);
    }


    //double month_0 = 0.03;
    /*for (int i = 0; i < 12; i++)
    {
        yield_quotes[i] = month_0;
        month_0 += 0.0054; // Ideally I wish to get this data from an excel file, preferably a live excel file.
    }

    for (int i = 0; i < 12; i++)
    {
        time_intervals[i] = (i*1.0 + 1.0);
    }

    for (int i = 0; i < 12; i++)
    {
        coupon_rates[i] = rand() % 10;
    }*/
}

/*void Market::yield_to_others()
{



    for (int i = 0; i < 11; i++)
    {
        forward_quotes[i] = (1 / (time_intervals[i+1] - time_intervals[i])) * ((zero_coupon_bond_price[i] / zero_coupon_bond_price[i + 1]) - 1);

    }

    for (int i = 0; i < 12; i++)
    {
        swap_quotes[i] = (zero_coupon_bond_price[0] - zero_coupon_bond_price[i]) / (12 * sum);
    }

}*/


void Market::display()
{
    std::cout << "Year\t\tYield Rate\t\tBond Price\t\tForward Rate\t\tSwap Rate\n\n" << std::endl;
    std::cout << std::fixed << std::setprecision(4);

    for (Futures& future : rates)
    {
        std::cout << future.year << "\t\t" << future.yield << "\t\t\t" << future.bond_price << "\t\t\t" << future.forward << "\t\t\t" << future.swap_rate << std::endl;

    }

   
}

