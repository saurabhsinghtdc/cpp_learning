#define _USE_MATH_DEFINES

#include<time.h>

#include "Vanilla_Option.h"
#include "Market.h"

#include <xlnt/xlnt.hpp>
#include <fstream>

int main(int argc, char** argv) {     
    xlnt::workbook book;
    book.load("trial.xlsx");
    auto ws = book.active_sheet();
    std::clog << "Processing spread sheet" << std::endl;

    std::cout << ws.rows(false)[0][1].to_string() << std::endl;

    for (auto row : ws.rows(false))
    {
        for (auto cell : row)
        {
            std::clog << cell.value<double>() <<"\t";
        }
        std::cout << std::endl;
    }
    std::clog << "Processing complete" << std::endl;
    srand(time(nullptr));

    Market m1 = Market(20);
   
    m1.display();
    /* Following is an example of an instance from Vanilla option class
    // First we create the parameter list
    std::cout << "Enter the following data in order: Stock price, Strike Price, risk-free interest rate, volatality of the stock, Time to maturity" << std::endl;
    double S, K, r, v, T = 0; // Option price

    std::cin >> S;
    std::cin >> K;
    std::cin >> r;
    std::cin >> v;
    std::cin >> T;

    // Then we calculate the call/put values
    double call = call_price(S, K, r, v, T);
    double put = put_price(S, K, r, v, T);
   
    // Finally we output the parameters and prices
    std::cout << "Underlying:      " << S << std::endl;
    std::cout << "Strike:          " << K << std::endl;
    std::cout << "Risk-Free Rate:  " << r << std::endl;
    std::cout << "Volatility:      " << v << std::endl;
    std::cout << "Maturity:        " << T << std::endl;
     


    
    Vanilla_Option c1 = Vanilla_Option();
    std::cin >> c1;
   double call = c1.call_price();
    double put = c1.put_price();
    std::cout << "The initial parameters are :\n" << c1 << std::endl;
    std::cout << "Call Price:      " << call << std::endl;
    std::cout << "Put Price:       " << put << std::endl;
    /*
    std::cout << "The current stock price is Company A is 100 USD, and the rate and Volatality of the stock are 5% and 20% respectively and the maturity is 1 year.\n";
    std::cout << "We will now compare the call and put prices for corresponding Strike prices.\n";
    c1.S_fix_K_mov();
     */
    return 0;
}