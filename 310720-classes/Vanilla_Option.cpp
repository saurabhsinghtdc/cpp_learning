
#include "Vanilla_Option.h"

#define _USE_MATH_DEFINES
#include <cmath>

#define M_PI       3.14159265358979323846

double Vanilla_Option::norm_pdf(const double& x)
{
    return (1.0 / (pow(2 * M_PI, 0.5))) * exp(-0.5 * x * x);

}

double Vanilla_Option::norm_cdf(const double& x)
{
    double k = 1.0 / (1.0 + 0.2316419 * x);
    double k_sum = k * (0.319381530 + k * (-0.356563782 + k * (1.781477937 + k * (-1.821255978 + 1.330274429 * k))));

    if (x >= 0.0) {
        return (1.0 - (1.0 / (pow(2 * M_PI, 0.5))) * exp(-0.5 * x * x) * k_sum);
    }
    else {
        return 1.0 - norm_cdf(-x);
    }

}

double Vanilla_Option::d_j(const int& j)
{
    return (log(S / K) + (r + (pow(-1, j - 1)) * 0.5 * v * v) * T) / (v * (pow(T, 0.5)));

}
/*
void Vanilla_Option::S_fix_K_mov()
{
    
    for (int i = 0; i < 10; i++)
    {
        S = 100;
        K = K_c[i];
        call_c[i] = call_price();
        put_c[i] = put_price();
        std::cout<<"Stock - 100; "<<"Strike - "<<K_c[i]<<"; Call Price- "<<call_c[i]<<"; Put Price- "<<put_c[i]<<std::endl;
    }
}
*/
double Vanilla_Option::call_price()
{
    return S * norm_cdf(d_j(1)) - K * exp(-r * T) * norm_cdf(d_j(2));

}

double Vanilla_Option::put_price()
{
    return -S * norm_cdf(-d_j(1)) + K * exp(-r * T) * norm_cdf(-d_j(2));

}

std::istream& operator>>(std::istream& input, Vanilla_Option& inst)
{
    std::cout << "Enter the following data in order: Stock price, Strike Price, risk-free interest rate, volatality of the stock, Time to maturity" << std::endl;


    input >> inst.S;
    input >> inst.K;
    input >> inst.r;
    input >> inst.v;
    input >> inst.T;

    return input; 

}

std::ostream& operator<<(std::ostream& output, Vanilla_Option& inst)
{
    output << "Underlying:      " << inst.S << std::endl;
    output << "Strike:          " << inst.K << std::endl;
    output << "Risk-Free Rate:  " << inst.r << std::endl;
    output << "Volatility:      " << inst.v << std::endl;
    output << "Maturity:        " << inst.T << std::endl;

    return output;
}

