#pragma once
#include<iostream>


class Vanilla_Option {
	


public:
	double S =100, K=100, r=0.05, v=0.20, T = 0;
	double norm_pdf(const double& x);
	double norm_cdf(const double& x);
	double d_j(const int& j);
	//double S_c = 10, K_c[10] = { 102, 104, 106, 108, 110, 112, 114, 116, 118, 120 }, r_c = 0.05, v_c = 0.20, T_c = 1, call_c[10], put_c[10];
	//void S_fix_K_mov();
	double call_price();
	double put_price();
	friend std::istream& operator>>(std::istream& input, Vanilla_Option& inst);
	friend std::ostream& operator<<(std::ostream& output, Vanilla_Option& inst);
	//friend std::istream& operator>>(std::istream& input, Vanilla_Option& inst);
};

