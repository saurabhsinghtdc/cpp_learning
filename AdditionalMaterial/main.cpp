#include <iostream>
#include <vector>
#include <algorithm>

#if defined(__WIN32__) || defined(_WIN32) || defined(WIN32) || defined(__WINDOWS__) || defined(__TOS_WIN__)
#define PLATFORM_WINDOWS
#elif defined(__LINUX__) || defined (__linux) || defined(linux) || defined(__linux__)
#define PLATFORM_LINUX
#elif defined(__ANDROID__) || defined (ANDROID)
#error "Why are you compiling this on Android!?!?"
#endif

#include "AbstractClasses/AbstractClassExample.h"

std::vector<Example*> examples;

bool isStringNumeric(const std::string& s) {
    return !s.empty() && std::find_if(s.begin(), 
        s.end(), [](unsigned char c) { return !std::isdigit(c); }) == s.end();
}

void toLower(std::string& s) {
    std::for_each(s.begin(), s.end(), [](char & c){
        c = std::tolower(c);
    });
}

void getInput() {
    std::cout << "Choose an example, type 'clear' to clear the console and 'exit' to exit the application" << std::endl;
    for(int i = 0; i < examples.size(); i++) {
        Example* example = examples[i];

        std::cout << (i + 1) << ". " << example->getName() << std::endl;
    }

    std::string input;
    std::cin >> input;

    toLower(input);

    if(input.compare("clear") == 0) {
        #ifdef PLATFORM_WINDOWS
            std::system("cls");
        #elif defined(PLATFORM_LINUX)
            std::system("clear");
        #endif

        getInput();
    } else if (input.compare("exit") == 0) {
        // We just exit, probably could have handled this more gracefuly
    } 
    else {
        bool valid = true;

        if(isStringNumeric(input)) {
            int selection = std::stoi(input);

            if(selection > 0 && selection <= examples.size()) {
                Example* example = examples[selection - 1];

                std::cout << "------------------------------ " << example->getName() << " ------------------------------" << std::endl;
                example->run();

                for(int i = 0; i < example->getName().size(); i++) {std::cout << "-";}
                std::cout << "--------------------------------------------------------------" << std::endl;
            }
        } else {
            valid = false;
        }

        if(!valid) {
            std::cout << "Error: Invalid Input: '" << input << "'" << std::endl;
        }

        getInput();
    }
}

int main(int, char**) {
    examples.push_back(new AbstractClassExample());

    getInput();
}