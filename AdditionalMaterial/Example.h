#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <string>

class Example {
public:
    virtual std::string getName() = 0;

    virtual void run() = 0;
};

#endif