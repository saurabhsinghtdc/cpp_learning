#include "../Example.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

class Animal {
public:
    virtual void makeSound() = 0;
};

class Dog : public Animal {
public:
    void makeSound() override {
        std::cout << "Woof" << std::endl;
    }
};

class Cat : public Animal {
public:
    void makeSound() override {
        std::cout << "Meow" << std::endl;
    }
};

class Cow : public Animal {
public:
    void makeSound() override {
        std::cout << "Moo" << std::endl;
    }
};

std::string getAnimalTypeString(Animal* animal) {
    if(dynamic_cast<Dog*>(animal) != nullptr) {
        return "Dog";
    } else if (dynamic_cast<Cat*>(animal) != nullptr) {
        return "Cat";
    } else if(dynamic_cast<Cow*>(animal) != nullptr) {
        return "Cow";
    }
    
    return "Unknown Animal";
}

class AbstractClassExample : public Example {
public:
    void run() override {
        /*
            Animal* animal = new Animal() - VALID
            Animal* animal = new Animal() - VALID
            Animal* animal = new Animal() - VALID
            Animal* animal = new Animal() - NOT VALID
        */

        // We can keep arrays of the base type
        Animal *animals[3];
        animals[0] = new Dog();
        animals[1] = new Cat();
        animals[2] = new Cow();

        for (Animal *animal : animals) {
            animal->makeSound();
        }

        /* 
            After we are done using the allocated objects, we have to delete them,
            else everytime we run this example we will leak memory
        */
        for (int i = 0; i < 3; i++) {
            delete animals[i];
        }

        // We can also more dynamically populate containers
        srand(time(0)); // Initialize a random seed

        const int animalCount = 10;
        Animal* randomAnimals[animalCount];
        // Generate 10 random animals
        for (int i = 0; i < animalCount; i++) {
            Animal *animal = nullptr;

            int num = rand() % 3 + 1; // Get a random number between 1 and 3

            if (num == 1)
                animal = new Dog();
            else if (num == 2)
                animal = new Cat();
            else if (num == 3)
                animal = new Cow();

            randomAnimals[i] = animal;
        }

        /* 
            Now lets make them make a sound and also print out the type of the animal
            since we can figure out which specific subclass of Animal the actual object is
            this is done in the function getAnimalTypeString()

            DISCLAIMER: checking specificaly what type the object is kind of defeats the purpose
            of having virtual classes and inheritance and the use of it could indicate poor design,
            but for the sake of example, this is possible
        */
        
        for(Animal* animal : randomAnimals) {
            std::cout << "A " << getAnimalTypeString(animal) << " says: ";
            animal->makeSound();
        }
       

        // Yet again, we clean up the memory after we are done
        for (int i = 0; i < animalCount; i++) {
            delete randomAnimals[i];
        }
    }

    std::string getName() override {
        return "Abstract Classes";
    }
};