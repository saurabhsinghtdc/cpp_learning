#include <iostream>

class Composite {
public:
    Composite() {
        std::cout << "constructing" << std::endl;
    }

    ~Composite() {

        std::cout << "destructing" << std::endl;
    }

    int a = 10;
    int b;
    int c;
};