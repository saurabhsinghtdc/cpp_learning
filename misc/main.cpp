#include <iostream>
#include <string>

class Animal {
public:
    virtual void makeSound() = 0;

    void say() {
        std::cout << "awefawe";   
    }

    std::string name;
};

class Dog : public Animal {
    public:
        void makeSound() override {
            std::cout << "Woof" << std::endl;
        }

};


class Cat : public Animal {
    public:
        void makeSound() override {
            std::cout << "Meow" << std::endl;
        }

};

int main(int, char**) {
    Animal* animal = nullptr;

    std::cout << "what animal do you like?" << std::endl;

    std::string choice = "";

    std::cin >> choice;

    if(choice.compare("dog") == 0) {
        animal = new Dog();

    }

    else if(choice.compare("cat") == 0) {

        animal = new Cat();
    }

    else {
        animal = new Dog();
    }





    animal->makeSound();
}